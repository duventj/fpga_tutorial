library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.tta0_imem_mau.all;
use work.tta0_imem_image.all;
use work.tta0_params.all;
use work.tta0_globals.all;

entity tutorial_processor1 is
  
  port (
    clk  : in  std_logic;
    rstx : in  std_logic;
    leds : out std_logic_vector(fu_leds_led_count-1 downto 0)
    );

end tutorial_processor1;

architecture structural of tutorial_processor1 is

  component tta0
    port (
      clk                : in  std_logic;
      rstx               : in  std_logic;
      busy               : in  std_logic;
      imem_en_x          : out std_logic;
      imem_addr          : out std_logic_vector(IMEMADDRWIDTH-1 downto 0);
      imem_data          : in  std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1 downto 0);
      pc_init            : in  std_logic_vector(IMEMADDRWIDTH-1 downto 0);
      fu_leds_led_output : out std_logic_vector(fu_leds_led_count-1 downto 0)); 
  end component;

  component inst_mem_logic
    generic (
      addrw  : integer := 10;
      instrw : integer := 100);
    port (
      clock   : in  std_logic;
      addr    : in  std_logic_vector(addrw-1 downto 0);
      dataout : out std_logic_vector(instrw-1 downto 0));
  end component;

  signal clk_w  : std_logic;
  signal rstx_w : std_logic;
  signal leds_w : std_logic_vector(fu_leds_led_count-1 downto 0);

  signal imem_addr_w : std_logic_vector(IMEMADDRWIDTH-1 downto 0);
  signal imem_data_w : std_logic_vector(IMEMWIDTHINMAUS*IMEMMAUWIDTH-1 downto 0);
  signal pc_init_w   : std_logic_vector(IMEMADDRWIDTH-1 downto 0);

  
begin  -- structural

  clk_w     <= clk;
  rstx_w    <= rstx;
  pc_init_w <= (others => '0');

  leds <= leds_w;

  imem : inst_mem_logic
    generic map (
      addrw  => IMEMADDRWIDTH,
      instrw => IMEMWIDTHINMAUS*IMEMMAUWIDTH)
    port map (
      clock   => clk_w,
      addr    => imem_addr_w,
      dataout => imem_data_w);

  -- If your synthesis tool doesn't allow open signals, connect open signals
  -- to dummy signals.
  core : tta0
    port map (
      clk                => clk_w,
      rstx               => rstx_w,
      busy               => '0',
      imem_en_x          => open,
      imem_addr          => imem_addr_w,
      imem_data          => imem_data_w,
      pc_init            => pc_init_w,
      fu_leds_led_output => leds_w);


end structural;
