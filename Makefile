


opset: led.obp
	buildopset led

asm: asm.tpef
	tceasm -o asm.tpef tutorial1.adf blink.tceasm

mem: blink.tpef
	tcecc -O3 -a tutorial2.adf -o blink.tpef blink_mem.c
	
all: opset asm mem 
	generateprocessor -i tutorial1.idf -o asm_vhdl/proge-output tutorial1.adf
	generatebits -f vhdl -p asm.tpef -x asm_vhdl/proge-output tutorial1.adf

clean:
	rm -rf *.tpef